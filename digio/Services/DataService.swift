//
//  DataService.swift
//  digio
//
//  Created by André Vicente on 30/09/20.
//

import Foundation
import RxSwift
import Alamofire

struct ApiError: Error {
    let message: String

    init(_ message: String) {
        self.message = message
    }

    public var localizedDescription: String {
        return message
    }
}

class DataService {

    static let shared = DataService()
    
    private let productsUrl = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products"
    
    func getProducts() -> Observable<AllProductsModel>{

        return Observable.create { (observer) -> Disposable in

            AF.request(self.productsUrl)
                .responseJSON { (response) in
                switch response.result {
                case .success:

                    guard let data = response.data else {
                        return
                    }

                    do {
                        let dataModel = try JSONDecoder().decode(AllProductsModel.self, from: data)

                        observer.onNext(dataModel)

                    } catch {
                        print("catch error: \(error) - \(data)");
                        observer.onError(error)
                    }
                case .failure(let error):
                    print("erro: \(response) \(error.localizedDescription)")
                    observer.onError(error)
                }
            }

            return Disposables.create()
        }
    }
}
