//
//  UIView+Shortcuts.swift
//  digio
//
//  Created by André Vicente on 03/10/20.
//

import UIKit

extension UIView {

    func setCardView(){

        self.backgroundColor = .white
        self.layer.cornerRadius = 10.0
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.7
    }
}
