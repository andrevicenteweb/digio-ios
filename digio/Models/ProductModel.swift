//
//  ProductModel.swift
//  digio
//
//  Created by André Vicente on 30/09/20.
//

import Foundation

struct AllProductsModel: Decodable {
    
    var products: [ProductModel]?
    var spotlights: [ProductModel]?
    var cash: ProductModel?
    
    init(){
        
    }
    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        products = try container.decode([ProductModel].self, forKey: .products)
        spotlights = try container.decode([ProductModel].self, forKey: .spotlight)
        cash = try container.decode(ProductModel.self, forKey: .cash)
    }

    enum CodingKeys: String, CodingKey {
        case products
        case spotlight
        case cash
    }
}

struct ProductModel: Codable {
    var name: String?
    var imageURL: String?
    var bannerURL: String?
    var description: String?
    
    func getImage() -> String{
        var imageUrl = "placeholder"
        
        if let image = self.imageURL{
            imageUrl = image
        }
        
        if let image = self.bannerURL{
            imageUrl = image
        }
        
        return imageUrl;
    }
}
