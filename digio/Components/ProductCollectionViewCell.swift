//
//  ProductCollectionViewCell.swift
//  digio
//
//  Created by André Vicente on 03/10/20.
//

import UIKit
import Kingfisher

class ProductCollectionViewCell: UICollectionViewCell {

    static var identifier = "ProductCollectionViewCell"
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var backgroundCell: UIView!
    
    var product: ProductModel? {
        
        didSet{
            guard let product = product else { return }
            
            image.image = nil
            let imageUrl = product.getImage()
            
            let url = URL(string: imageUrl)
            image.kf.setImage(
                with: url,
                placeholder: UIImage(named: "placeholder")
            )
            
            backgroundCell.setCardView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
         
    }

}
