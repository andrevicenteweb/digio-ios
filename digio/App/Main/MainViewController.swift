//
//  MainViewController.swift
//  digio
//
//  Created by André Vicente on 30/09/20.
//

import UIKit
import RxSwift
import RxCocoa
import FSPagerView
import Kingfisher

class MainViewController: UIViewController, Storyboarded {
    
    var viewModel: MainViewModel?
    var spotlightArray: [ProductModel] = [];
    var disposeBag = DisposeBag()
    
    @IBOutlet weak var pagerViewSpotlight: FSPagerView! {
        didSet {
            self.pagerViewSpotlight.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    
    @IBOutlet weak var imageViewCash: UIImageView!
    
    @IBOutlet weak var collectionViewProducts: UICollectionView!
    
    @IBOutlet weak var constraintImageHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupPagerViews()
        self.registerCell()
        self.setupBindings()
    }
    
    private func setupPagerViews(){
        
        let itemSpace: CGFloat = 12.0
        
        let transform = CGAffineTransform(scaleX: 0.8, y: 1)
        pagerViewSpotlight.itemSize = pagerViewSpotlight.frame.size.applying(transform)
        pagerViewSpotlight.decelerationDistance = FSPagerView.automaticDistance
        pagerViewSpotlight.interitemSpacing = itemSpace
        pagerViewSpotlight.dataSource = self
        pagerViewSpotlight.delegate = self
    }
    
    func registerCell(){
        
        let customCell = UINib(nibName: ProductCollectionViewCell.identifier, bundle: nil)
        collectionViewProducts.register(customCell, forCellWithReuseIdentifier: ProductCollectionViewCell.identifier)
        collectionViewProducts.contentInset = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 25);
    }
    
    private func setupBindings(){
        
        guard let viewModel = self.viewModel else { return }
        
        viewModel.spotlightArray
            .asObserver()
            .subscribe(onNext: { spotlights in
                self.spotlightArray = spotlights
                self.pagerViewSpotlight.reloadData()
                
            }).disposed(by: disposeBag)
        
        viewModel.cashObject
            .asObserver()
            .subscribe(onNext: { cash in
                self.setCashImage(product: cash)
            }).disposed(by: disposeBag)
        
        viewModel.productsArray.asObserver()
            .bind(to: collectionViewProducts.rx.items(cellIdentifier: ProductCollectionViewCell.identifier, cellType: ProductCollectionViewCell.self)){ row, product, cell in
                
                cell.product = product
                
        }.disposed(by: disposeBag)
    }
    
    private func setCashImage(product: ProductModel){
        imageViewCash.image = nil
        
        let imageUrl = product.getImage()
        
        let url = URL(string: imageUrl)
        imageViewCash.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholder")
        )
        
        if let image = imageViewCash.image{
            let ratio = image.size.width / image.size.height
            
            let newHeight = imageViewCash.frame.height * ratio
            constraintImageHeight.constant = newHeight
            
            view.layoutIfNeeded()
        }
    }
}

extension MainViewController : FSPagerViewDelegate {
    func pagerView(_ pagerView: FSPagerView, shouldHighlightItemAt index: Int) -> Bool{
        return false
    }
}

extension MainViewController : FSPagerViewDataSource {
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return spotlightArray.count
    }
        
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        let product = spotlightArray[index]
        
        cell.imageView?.image = nil
        cell.imageView?.contentMode = .scaleAspectFit
        
        let imageUrl = product.getImage()
        
        let url = URL(string: imageUrl)
        cell.imageView?.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholder")
        )
        
        return cell
    }
}
