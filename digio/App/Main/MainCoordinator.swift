//
//  MainCoordinator.swift
//  digio
//
//  Created by André Vicente on 30/09/20.
//

import Foundation
import RxSwift

class MainCoordinator: BaseCoordinator {
    
    var viewController: MainViewController!
    var disposeBag = DisposeBag()
    
    override func start() {
          
        viewController = MainViewController.instantiate()
        
        let mainViewModel = MainViewModel(dataService: DataService())
        mainViewModel.loadData()
        viewController.viewModel = mainViewModel
            
        self.navigationController.navigationBar.isHidden = true
        self.navigationController.viewControllers = [viewController]
    }
}
