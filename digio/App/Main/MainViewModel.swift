//
//  MainViewModel.swift
//  digio
//
//  Created by André Vicente on 30/09/20.
//

import Foundation
import RxSwift

class MainViewModel{
    
    var spotlightArray = PublishSubject<[ProductModel]>()
    var productsArray = PublishSubject<[ProductModel]>()
    var cashObject = PublishSubject<ProductModel>()
    
    var dataService: DataService
    var disposeBag = DisposeBag()
    
    init(dataService: DataService) {
        self.dataService = dataService
    }
    
    func loadData(){
        
        dataService.getProducts()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { allProducts in
                
                if let products = allProducts.products{
                    self.productsArray.onNext(products)
                }
                
                if let spotlights = allProducts.spotlights{
                    self.spotlightArray.onNext(spotlights)
                }
                
                if let cash = allProducts.cash{
                    self.cashObject.onNext(cash)
                }
                
        }, onError: { error in
            
        }).disposed(by: self.disposeBag)
    }
    
    func detailsFrom(product: ProductModel){
        //self.movieDetails.onNext(movie)
    }
}
