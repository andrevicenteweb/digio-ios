//
//  AppCoordinator.swift
//  digio
//
//  Created by André Vicente on 30/09/20.
//

import UIKit

class AppCoordinator: BaseCoordinator {

    init(navigationController: UINavigationController) {
        super.init()
        navigationController.navigationBar.tintColor = .white
        self.navigationController = navigationController
    }

    override func start() {
        
        goToMainPage()
    }
}

protocol CoordinatorPageNavigation {
    func goToMainPage()
    //func goToMoviesPage()
    //func goToMovieDetails(_ movie: MovieModel)
}

extension AppCoordinator: CoordinatorPageNavigation {
    
    func goToMainPage(){
        let coordinator = MainCoordinator()
        coordinator.navigationController = self.navigationController
        self.start(coordinator: coordinator)
    }
//
//    func goToMoviesPage() {
//        let coordinator = MoviesCoordinator()
//        coordinator.navigationController = self.navigationController
//        self.start(coordinator: coordinator)
//    }
//
//    func goToMovieDetails(_ movie: MovieModel){
//
//        let coordinator = MovieDetailsCoordinator()
//        coordinator.movieSelected = movie
//        coordinator.navigationController = self.navigationController
//        self.start(coordinator: coordinator)
//    }
}
