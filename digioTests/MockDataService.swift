//
//  MockDataService.swift
//  digioTests
//
//  Created by André Vicente on 03/10/20.
//

import RxSwift
@testable import digio

class MockDataService: DataService{
    
    var mockAllProducts: AllProductsModel = AllProductsModel()
    
    override init() {

        let products = [
            ProductModel(name: "Test prod 1", imageURL: "image.png", bannerURL: nil, description: "Desc prod 1"),
            ProductModel(name: "Test prod 2", imageURL: "image.png", bannerURL: nil, description: "Desc prod 2"),
            ProductModel(name: "Test prod 3", imageURL: "image.png", bannerURL: nil, description: "Desc prod 3")
        ]
        
        self.mockAllProducts.products = products
        
        let spotlights = [
            ProductModel(name: "Test spotlight 1", imageURL: nil, bannerURL: "image.png", description: "Desc spotlight 1"),
            ProductModel(name: "Test spotlight 2", imageURL: nil, bannerURL: "image.png", description: "Desc spotlight 2")
        ]
        self.mockAllProducts.spotlights = spotlights
        
        self.mockAllProducts.cash = ProductModel(name: "Test Cash", imageURL: nil, bannerURL: "image.png", description: "Desc cash")
    }
    
    override func getProducts() -> Observable<AllProductsModel>{
        return Observable.just(self.mockAllProducts)
    }
}
