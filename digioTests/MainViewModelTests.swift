//
//  testMainViewModel.swift
//  digioTests
//
//  Created by André Vicente on 03/10/20.
//

import XCTest
import RxSwift
@testable import digio

class MainViewModelTests: XCTestCase {

    var dataService : MockDataService!
    var sut : MainViewModel!
    var disposeBag : DisposeBag!
    
    override func setUpWithError() throws {
        dataService = MockDataService()
        sut = MainViewModel(dataService: dataService)
        disposeBag = DisposeBag()
    }

    override func tearDownWithError() throws {
        dataService = nil
        sut = nil
        disposeBag = nil
    }

    func testFetchProducts(){
        
        sut.productsArray
            .asObserver()
            .subscribe(onNext: { products in
                XCTAssertEqual(products.count, 3)
            }, onError: { error in
                print("**** ERROR ")
            }).disposed(by: self.disposeBag)
        
        sut.loadData()
    }
    
    func testFetchSpotlight(){
        
        sut.spotlightArray
            .asObserver()
            .subscribe(onNext: { spotlight in
                XCTAssertEqual(spotlight.count, 2)
            }, onError: { error in
                print("**** ERROR ")
            }).disposed(by: self.disposeBag)
        
        sut.loadData()
    }
    
    func testFetchCash(){
        
        sut.cashObject
            .asObserver()
            .subscribe(onNext: { cash in
                XCTAssertEqual(cash.name, "Test Cash")
                XCTAssertEqual(cash.imageURL, nil)
                XCTAssertEqual(cash.bannerURL, "image.png")
                XCTAssertEqual(cash.description, "Desc cash")                
            }, onError: { error in
                print("**** ERROR ")
            }).disposed(by: self.disposeBag)
        
        sut.loadData()
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
